# yew-web

#### Description

rust web 前端开发库 yew，构建开发脚手架。

#### Gitee Feature

1.  You can use Readme_XXX.md to support different languages, such as Readme_en.md, Readme_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
