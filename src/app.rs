use yew::prelude::*;
use yew_router::prelude::*;

//
use crate::routes::route::{switch, Route};
use crate::stores::app;

#[function_component]
pub fn App() -> Html {
    // let app = use_reducer(|| app::App {
    //     name: "yew-web".to_string(),
    // });

    html! {
        <BrowserRouter>
            // <ContextProvider<app::App> context={(*app).clone()} >
            //     <Switch<Route> render={switch} />
            // </ContextProvider<app::App>>
            <app::AppProvider>
                <Switch<Route> render={switch} />
            </app::AppProvider>
        </BrowserRouter>
    }
}
