use yew::prelude::*;

use crate::stores::app;

#[function_component]
pub fn App() -> Html {
    let context = use_context::<app::AppContext>().unwrap();

    let app_name = context.name.to_owned();

    html! {
        <div class="user">
            <h2>{"个人中心"}</h2>

            <p>{"消费来自根部组件的数据："}{app_name}</p>
        </div>
    }
}
