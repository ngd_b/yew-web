use yew::prelude::*;
use yew_router::hooks::use_navigator;

use crate::routes::route::Route;
use crate::stores::app;

#[function_component]
pub fn App() -> Html {
    let navigator = use_navigator().unwrap();
    let app = use_context::<app::AppContext>().unwrap();

    let handle_logout = {
        Callback::from(move |_| {
            navigator.push(&Route::Login);
        })
    };
    let handle_update_app = {
        Callback::from(move |_| {
            app.dispatch("trunk-yew".to_string());
        })
    };
    html! {
        <div class="hb-header">
            <h2 class="header-title">{"trunk-yew"}</h2>

            <div class="header-tools">
                <button class="hb-button" onclick={handle_update_app}>{"更新"}</button>
                <button class="hb-button close" onclick={handle_logout}>{"退出"}</button>
            </div>
        </div>
    }
}
