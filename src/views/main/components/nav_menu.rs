use yew::prelude::*;
use yew_router::prelude::*;

use crate::routes::main::MainRoute;

#[function_component]
pub fn App() -> Html {
    let route: Option<_> = use_route::<MainRoute>();
    let active_menu = use_state(|| MainRoute::Home);
    let active_menu_set = active_menu.clone();

    let is_active = |route: MainRoute| {
        let mut class = classes!("menu-item");
        let menu = active_menu.clone();

        if route == *menu {
            class.push("active");
        }
        class
    };

    // let change_active = move |route: MainRoute| {
    //     active_menu_set.set(route);
    // };
    use_effect_with(route.clone(), move |_| {
        // ..
        let route = route.clone();
        if let Some(active_route) = route {
            // change_active(active_route);
            active_menu_set.set(active_route);
        }
    });

    html! {
        <div class="hb-nav-menu">
            <div class="nav-menu">
                <Link<MainRoute> classes={is_active(MainRoute::Home)} to={MainRoute::Home}>{"首页"}</Link<MainRoute>>
                <Link<MainRoute> classes={is_active(MainRoute::User)} to={MainRoute::User}>{"用户"}</Link<MainRoute>>
                <Link<MainRoute> classes={is_active(MainRoute::CNode)} to={MainRoute::CNode}>{"CNode"}</Link<MainRoute>>
            </div>
        </div>
    }
}
