use yew::prelude::*;
use yew_router::prelude::*;
mod components;

use crate::routes::main::{main_switch, MainRoute};
use components::{header, nav_menu};

#[function_component]
pub fn App() -> Html {
    html! {
        <div class="hb-main">
            <header::App></header::App>
            <div class="hb-main-layout">
                <nav_menu::App></nav_menu::App>
                <div class="hb-main-content">
                    <Switch<MainRoute> render={main_switch} />
                </div>
            </div>
        </div>
    }
}
