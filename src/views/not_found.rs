use yew::prelude::*;

#[function_component]
pub fn App() -> Html {
    html! {
        <div class="not-fount">
            <h2>{"页面无法访问"}</h2>
        </div>
    }
}
