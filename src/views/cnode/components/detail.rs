use serde::Deserialize;

#[derive(Deserialize, Debug, Clone, PartialEq)]
pub struct Detail {
    pub id: String,
    pub title: String,
    pub top: bool,
    pub visit_count: i32,
    pub content: String,
    pub create_at: String,
}
impl Detail {
    pub fn new() -> Self {
        Self {
            id: "".to_string(),
            title: "".to_string(),
            top: false,
            visit_count: 0,
            content: "".to_string(),
            create_at: "".to_string(),
        }
    }
}

// 请求响应
#[derive(Deserialize, Debug, Clone, PartialEq)]
pub struct DetailResponse {
    pub success: bool,
    pub data: Detail,
}
