use serde::{Deserialize, Serialize};

/**
 * 请求参数
 */
#[derive(Serialize, Debug, Clone, PartialEq)]
pub struct TopicRequest {
    pub page: i32,
    pub tab: String,
    pub limit: i32,
    pub mdrender: bool,
}

// 数据主体
#[derive(Deserialize, Debug, Clone, PartialEq)]
pub struct Topic {
    pub id: String,
    pub title: String,
    pub top: bool,
    pub visit_count: i32,
    pub content: String,
    pub create_at: String,
}

// 请求响应
#[derive(Deserialize, Debug, Clone, PartialEq)]
pub struct TopicResponse {
    pub success: bool,
    pub data: Vec<Topic>,
}
