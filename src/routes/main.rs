use yew::prelude::*;
use yew_router::prelude::*;

// component
use crate::routes::route::Route;
use crate::views::cnode::{detail, list};
use crate::views::user;

#[derive(Clone, Routable, PartialEq)]
pub enum MainRoute {
    #[at("/main")]
    Home,
    #[at("/main/user")]
    User,
    #[at("/main/cnode")]
    CNode,
    #[at("/main/cnode/detail/:id")]
    CNodeDetail { id: String },
    #[not_found]
    #[at("/main/404")]
    NotFound,
}

pub fn main_switch(routes: MainRoute) -> Html {
    match routes {
        MainRoute::Home => html! {<h1>{"首页"}</h1>},
        MainRoute::User => html! {<user::App />},
        MainRoute::CNode => html! {<list::App />},
        MainRoute::CNodeDetail { id } => html! {<detail::App id={id.clone()} />},
        MainRoute::NotFound => html! {<Redirect<Route> to={Route::NotFound} />},
    }
}
