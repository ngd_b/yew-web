use yew::prelude::*;
use yew_router::prelude::*;

// component
use crate::views::login;
use crate::views::main;
use crate::views::not_found;

use crate::routes::main::MainRoute;

#[derive(Clone, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Home,
    #[at("/main")]
    MainRoot,
    #[at("/main/*")]
    Main,
    #[at["/login"]]
    Login,
    #[not_found]
    #[at("/404")]
    NotFound,
}

pub fn switch(routes: Route) -> Html {
    // log!(JsValue::from(routes));
    match routes {
        Route::Home => html! {<Redirect<MainRoute> to={MainRoute::Home}/>},
        Route::Main | Route::MainRoot => html! {<main::App />},
        Route::Login => html! {<login::App />},
        Route::NotFound => html! {<not_found::App />},
    }
}
