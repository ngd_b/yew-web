use log::info;
use std::rc::Rc;
use yew::prelude::*;

#[derive(Clone, Debug, PartialEq)]
pub struct App {
    pub name: String,
}

impl Reducible for App {
    type Action = String;

    fn reduce(self: Rc<Self>, action: Self::Action) -> Rc<Self> {
        info!("action--{:?}", action);
        App { name: action }.into()
    }
}

pub type AppContext = UseReducerHandle<App>;

#[derive(Properties, Debug, PartialEq)]
pub struct AppProviderProps {
    #[prop_or_default]
    pub children: Html,
}

#[function_component]
pub fn AppProvider(props: &AppProviderProps) -> Html {
    let app = use_reducer(|| App {
        name: "yew-web".to_string(),
    });
    html! {
        <ContextProvider<AppContext> context={app}>
            {props.children.clone()}
        </ContextProvider<AppContext>>
    }
}
