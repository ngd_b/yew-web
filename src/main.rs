use log::Level;
use std::panic;

mod ajax;
mod app;
mod routes;
mod stores;
mod views;
//
use app::App;

fn main() {
    panic::set_hook(Box::new(console_error_panic_hook::hook));
    let _ = console_log::init_with_level(Level::Debug);

    yew::Renderer::<App>::new().render();
}
